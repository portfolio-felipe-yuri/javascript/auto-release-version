// Requires, Imports and Configs
require("dotenv").config();
const axios = require("axios");
const log = require("log-beautify");
const { create } = require('xmlbuilder2');

// Variables and Constants
const METHOD_GET = "get";
const METHOD_POST = "post";
const METHOD_PUT = "put";
const SUFFIX_GA = "";
const SUFFIX_DEV = "-DEV";
const SUFFIX_RC = "-RC";
const REGEX = /\d+\.\d+/;

let version = process.env.branch || erro("Versão não informada!");
version = filterVersion(version);
let ref = process.env.ref || log.info("Branch de origem não informada! Essa branch será copia da versão anterior.\n");
let project_ids = trimAll(process.env.PROJECTS_IDS) || erro("Lista de projetos não informada!");


// Functions
async function createBranch(branchName, project_id, ref) {
  if (ref == undefined || ref == null || ref == "") {
    ref = getPreviousVersion(branchName);
  }

  const url_getBranches = `${process.env.HOST_GITLAB}/${project_id}/repository/branches`;
  const resp = await gitlabApiService(url_getBranches, METHOD_GET);
  const toCreate = isValidBranch(resp, branchName, project_id);

  if (toCreate) {
    isValidBranch(resp, ref, project_id, true);

    const url_createBranch = `${url_getBranches}?branch=${branchName}&ref=${ref}`;

    log.info_(`Parâmetros de Entrada`);
    log.info(`BranchName: ${branchName}`);
    log.info(`ProjectID: ${project_id}`);
    // log.info(`ProjectName: ${process.env.CI_PROJECT_NAME}`);
    log.info(`BranchOrigem: ${ref}`);
    log.info(`URL: ${url_createBranch}\n\n`);

    await gitlabApiService(url_createBranch, METHOD_POST);
  }
}

function isValidBranch(resp, branchName, project_id, isRef = false) {
  respData = resp.data;

  if (isRef && !respData.some((item) => {
      return item.name == branchName;
    })
  ) {
    erro(`Não existe a branch de origem '${branchName}' no projeto com id '${project_id}'\n`);
  }
  if (!isRef && respData.some((item) => {
      return item.name == branchName;
    })
  ) {
    log.info(`A branch '${branchName}' já existe no projeto com id '${project_id}'\n`);
    return false;
  }
  return true;
}

async function gitlabApiService(url_, method, data) {
  const requestBody = {
    method: method,
    url: url_,
    responseType: "json",
    headers: { "PRIVATE-TOKEN": process.env.GITLAB_API_TOKEN, 'Content-Type': 'application/json' },
    data: data,
  };
  let resp;
  await axios(requestBody).then(function (response) {
      log.success(`Serviço executado com sucesso para a url: ${url_}\n`);
      resp = response;
    }).catch(function (error) {
      erro(error);
    });

  return resp;
}
async function jenkinsApiService(url_, method) {
  const requestBody = {
    method: method,
    url: url_,
    headers: { 
      "Content-Type": "application/json", 
      "Authorization": process.env.JENKINS_API_TOKEN },
  };
  let resp;
  await axios(requestBody).then(function (response) {
      log.success(`Serviço executado com sucesso para a url: ${url_}\n`);
      resp = response;
    }).catch(function (error) {
      erro(error);
    });
  return resp;
}

function getPreviousVersion(branchName) {
  const version = filterVersion(branchName);
  const verisonPrevious = ((Number(version * 100) - 1) / 100).toFixed(2);
  return verisonPrevious;
}

function erro(mensagem) {
  log.error(`ERRO! => ${mensagem}`);
  process.exit(1);
}

async function buildJenkinsJob(version) {
  const url = `${process.env.HOST_JENKINS}/job/${version}/buildWithParameters`;
  await jenkinsApiService(url, METHOD_POST);
}

async function createJenkinsJobFrom(version) {
  const url = `${process.env.HOST_JENKINS}/createItem?name=${version}&mode=copy&from=${process.env.JOB_VIRADAVERSAO}`;
  await jenkinsApiService(url, METHOD_POST);
}

function filterVersion(version) {
  version = version.match(REGEX);
  return version[0];
}

async function addJenkinsJobToView(version, view) {
  const url = `${process.env.HOST_JENKINS}/view/${view}/addJobToView?name=${version}`;
  await jenkinsApiService(url, METHOD_POST);
}

function trimAll(str) {
  const array = str.split(",").map(item => {
    return item.trim();
  });
  return array;
}

async function getCalendarFile(file, branch) {
  const url = `${process.env.HOST_GITLAB}/${process.env.companyWBUILD_ID}/repository/files/${file}?ref=${branch}`
  return await gitlabApiService(url, METHOD_GET);
}

function getCalendarContent(file) {
  return decode(file.data.content);
}

function decode(base64) {
  const buff = Buffer.from(base64, 'base64');
  return buff.toString('utf-8'); 
}

function encode(str) {
  const buff = Buffer.from(str, 'utf-8');
  return buff.toString('base64'); 
}

function insertCalendarElement(content, version, creationDate, releaseDate) {
  const doc = create(content);
  const versaoElement = doc.root().ele('versao');
  versaoElement.ele('skwVersion').txt(version).up();
  versaoElement.ele('creationDate').txt(creationDate).up();
  versaoElement.ele('releaseDate').txt(releaseDate).up();
  return doc.end({ prettyPrint: true });
}

function getNow() {
  return new Date().toLocaleDateString();
}

function getReleaseDate() {
  return new Date(Date.now() + 31 * 24 * 60 * 60 * 1000).toLocaleDateString();
}

async function updateFileFromRepository(file, data) {
  const url = `${process.env.HOST_GITLAB}/${process.env.companyWBUILD_ID}/repository/files/${file}`;
  return await gitlabApiService(url, METHOD_PUT, data);
}

function buildData(branch, content, version) {
  const data = JSON.stringify({
    "branch": branch,
    "content": content,
    "commit_message": `Inserindo a versao ${version}`
  });
  return data;
}

async function execute() {
  const branchSuffix = [SUFFIX_GA, SUFFIX_DEV, SUFFIX_RC];

  for (let x = 0; x < branchSuffix.length; x++) {

    const ver = version.concat(branchSuffix[x]);

    for (let i = 0; i < project_ids.length; i++) {
      let id = project_ids[i];
      await createBranch(ver, id, ref);
      // await protectBranches(ver);
    }

    await createJenkinsJobFrom(version);
    // Aguardando reunião com Lucas Marcolino para pegar as permissões ou outra solução.
    // await createDB(version);

    if (x == 0) {
      const views = trimAll(process.env.VIEWS);
      const filePath = trimAll(process.env.CALENDAR_FILE_PATH);
      const calendarFile = await getCalendarFile(filePath, process.env.companyWBUILD_BRANCH);
      const calendarContent = await getCalendarContent(calendarFile);
      const newCalendarXml = insertCalendarElement(calendarContent, version, getNow(), getReleaseDate());
      // const calendarEncoded = encode(newCalendarXml);
      const data = buildData(process.env.companyWBUILD_BRANCH, newCalendarXml, version);
      await updateFileFromRepository(filePath, data);

      for (let z = 0; z < views.length; z++) {
        await addJenkinsJobToView(version, views[z]);
      }
    }
    await buildJenkinsJob(version);
  }

  log.success("Virada de versão realizada com sucesso!!!")
}

// Execute
execute();
